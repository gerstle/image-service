# image-service

image-service provides a simple API to perform the following operations:

* Fetch a list of images
    * Supports paging (offset, limit query string parameters). Default offset of zero and limit of 500
    * Can be limited to those for a specific patient
* Fetch an individual image's details
* Fetch the full-size version of a given image
* Fetch the thumbnail version of a given image

## Running

With Java-10 and Maven installed, simply run `./start.sh` from the base directory of the project. This will run locally using the `local` profile and make the API available on `localhost:8080`

## Authorization

* The service is protected by Basic authorization. A header search as `Authorization: Basic <encoded username:password>` should be included on all requests.
* Further, a `context` query string parameter is required on each request and this defines the context in which the user is operating. Eg. `context=hospital/1`
    * For super users, `all` can be used for the ID parameter. Eg. `context=hospital/all`

## API

### Fetch details for images : GET /api/v1/images?{context,offset,limit,patientId}

#### parameters

* context: required. Values should be of the form <entity>/<id>. Currently valid entities are `hospital`. Id should be an ID of a hospital or `all` (admins only)
* offset: optional. Zero based offset for the records returned. Used for paging.
* limit: optional. Max number of records to return. Defaults to 500
* patientId: optional. Limit returned records to those just for the patient.

#### Request

n/a

#### Response

* JSON object with the following fields:
* offset: the offset used in the request
* responseCount: number of records returned in response
* entities: array of Image records
    * Image object fields
        * id: id of record
        * patientId: patient Id
        * name: image name
        * description: description of image
        * filename: original filename of image
        * status: Status of image... ACTIVE, INACTIVE, UNKNOWN
        * version: Version of record, used to prevent unintended record update overwrites
        * created: timestamp of when record was created
        * created_by: user Id that created the record
        * modified: timestamp of when record was last updated
        * modified_by: user Id that updated the record

#### Example

```
GET http://localhost:8080/api/v1/images?context=hospital/1&patientId=2
Authorization: Basic ZG9jdG9yQGhvc3BpdGFsb25lLmNvbTpib2F0ZHJpbmtz

Response 200 OK
--
{
    "offset": 0,
    "responseCount": 2,
    "entities": [
        {
            "id": 11,
            "patientId": 2,
            "name": "x-ray",
            "description": "patient 2 x-ray",
            "filename": "2_1.jpg",
            "status": "ACTIVE",
            "version": 0,
            "created": 1540334951047,
            "modified": 1540334951047,
            "created_by": 1,
            "modified_by": 1
        },
        {
            "id": 12,
            "patientId": 2,
            "name": "x-ray",
            "description": "patient 2 x-ray 2",
            "filename": "2_2.jpg",
            "status": "ACTIVE",
            "version": 0,
            "created": 1540334951047,
            "modified": 1540334951047,
            "created_by": 1,
            "modified_by": 1
        }
    ]
}

```

### Fetch image details : GET /api/v1/images/{id}?{context}

#### parameters

* id: required path parameter. Id of image to fetch
* context: required. Values should be of the form <entity>/<id>. Currently valid entities are `hospital`. Id should be an ID of a hospital or `all` (admins only)

#### Request

n/a

#### Response

* JSON object with the following fields:
    * id: id of record
    * patientId: patient Id
    * name: image name
    * description: description of image
    * filename: original filename of image
    * status: Status of image... ACTIVE, INACTIVE, UNKNOWN
    * version: Version of record, used to prevent unintended record update overwrites
    * created: timestamp of when record was created
    * created_by: user Id that created the record
    * modified: timestamp of when record was last updated
    * modified_by: user Id that updated the record

#### Example

```
GET http://localhost:8080/api/v1/images/11?context=hospital/1
Authorization: Basic ZG9jdG9yQGhvc3BpdGFsb25lLmNvbTpib2F0ZHJpbmtz

Response 200 OK
--
{
    "id": 11,
    "patientId": 2,
    "name": "x-ray",
    "description": "patient 2 x-ray",
    "filename": "2_1.jpg",
    "status": "ACTIVE",
    "version": 0,
    "created": 1540334951047,
    "modified": 1540334951047,
    "created_by": 1,
    "modified_by": 1
}

```

### Fetch full-size image : GET /api/v1/images/{id}/full?{context}

#### parameters

* id: required path parameter. Id of image to fetch
* context: required. Values should be of the form <entity>/<id>. Currently valid entities are `hospital`. Id should be an ID of a hospital or `all` (admins only)

#### Request

n/a

#### Response

* 200 OK w/ `Content-Type: image/jpeg` OR 204 No Content (exception situation)

#### Example

```
GET http://localhost:8080/api/v1/images/11/full?context=hospital/1
Authorization: Basic ZG9jdG9yQGhvc3BpdGFsb25lLmNvbTpib2F0ZHJpbmtz

Response 200 OK
--
<actual image content>
```

### Fetch thumbnail image : GET /api/v1/images/{id}/thumbnail?{context}

#### parameters

* id: required path parameter. Id of image to fetch
* context: required. Values should be of the form <entity>/<id>. Currently valid entities are `hospital`. Id should be an ID of a hospital or `all` (admins only)

#### Request

n/a

#### Response

* 200 OK w/ `Content-Type: image/jpeg` OR 204 No Content (exception situation)

#### Example

```
GET http://localhost:8080/api/v1/images/11/thumbnail?context=hospital/1
Authorization: Basic ZG9jdG9yQGhvc3BpdGFsb25lLmNvbTpib2F0ZHJpbmtz

Response 200 OK
--
<actual image content>
```
