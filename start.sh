#!/bin/bash
export MAVEN_OPTS="-Xms128m -Xmx128m -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=5005,suspend=n -Dspring.profiles.active=local -Djava.security.egd=file:/dev/./urandom"
mvn spring-boot:run
