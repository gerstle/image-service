FROM openjdk:10-jre-slim
VOLUME /tmp
ADD target/image-service-1.0-SNAPSHOT.jar app.jar
EXPOSE 8080
VOLUME /logs
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
