DROP TABLE IF EXISTS hospitals;
CREATE TABLE IF NOT EXISTS hospitals (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL
);

CREATE TABLE IF NOT EXISTS patients (
  id INT AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(256) NOT NULL,
  last_name VARCHAR(256) NOT NULL,
  dob DATETIME NOT NULL,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL
);

CREATE TABLE IF NOT EXISTS patient_hospitals (
  id INT AUTO_INCREMENT PRIMARY KEY,
  patient_id INT NOT NULL,
  hospital_id INT NOT NULL,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL,
  FOREIGN KEY (patient_id) REFERENCES patients(id),
  FOREIGN KEY (hospital_id) REFERENCES hospitals(id)
);

CREATE TABLE IF NOT EXISTS images (
  id INT AUTO_INCREMENT PRIMARY KEY,
  patient_id INT NOT NULL,
  name VARCHAR(256) NOT NULL,
  description TEXT NOT NULL,
  filename VARCHAR(2048) NOT NULL,
  relative_path VARCHAR(2048) NOT NULL,
  thumbnail_relative_path VARCHAR(2048) NOT NULL,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL
);

-- Access management stuff, ideally a completely separate system
-- allowed: JSON array of "<context>/<id>" strings for the data that this role has access to
-- denied: JSON array of "<context>/<id>" strings for the data that this role does NOT have access to
-- NOTE: The id for the allowed/denied strings can be a wildcard (*)
-- NOTE: The denied string trumps the allowed string
CREATE TABLE IF NOT EXISTS roles (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(256) NOT NULL,
  description TEXT NOT NULL,
  allowed TEXT,
  denied TEXT,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL
);

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(256) NOT NULL,
  password VARCHAR(1024) NOT NULL,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL
);

CREATE TABLE IF NOT EXISTS user_roles (
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  role_id INT NOT NULL,
  status VARCHAR(32)  NOT NULL,
  version INT NOT NULL ,
  created TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  created_by BIGINT NOT NULL,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by BIGINT  NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (role_id) REFERENCES roles(id)
);

