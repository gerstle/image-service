-- ROLES --
INSERT INTO roles ("NAME", "DESCRIPTION", "ALLOWED", "DENIED", "STATUS", "VERSION", "CREATED", "CREATED_BY", "MODIFIED", "MODIFIED_BY")
VALUES ('super user role', 'allows access to all hospital data', '["hospital/*"]', NULL, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('hospital one role', 'allows access to the hospital one data', '["hospital/1"]', NULL, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('hospital two role', 'allows access to the hospital two data', '["hospital/2"]', NULL, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);


-- USERS --
INSERT INTO users ("EMAIL", "PASSWORD", "STATUS", "VERSION", "CREATED", "CREATED_BY", "MODIFIED", "MODIFIED_BY")
VALUES ('admin@invalid.com', '$2a$12$VSrZt9COFNATh4XWzc7aQeOGZ0TfmWJTJjvRof05p4hN7xvLBxjDm', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('doctor@hospitalone.com', '$2a$12$VSrZt9COFNATh4XWzc7aQeOGZ0TfmWJTJjvRof05p4hN7xvLBxjDm', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('doctor@hospitaltwo.com', '$2a$12$VSrZt9COFNATh4XWzc7aQeOGZ0TfmWJTJjvRof05p4hN7xvLBxjDm', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);


-- USER-ROLES --
INSERT INTO USER_ROLES ("USER_ID", "ROLE_ID", "STATUS", "VERSION", "CREATED", "CREATED_BY", "MODIFIED", "MODIFIED_BY")
VALUES (1, 1, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (2, 2, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (3, 3, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);

-- HOSPITALS --
INSERT INTO hospitals (name, status, version, created, created_by, modified, modified_by)
VALUES ('hospital one', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('hospital two', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);


-- PATIENTS --
INSERT INTO patients ("FIRST_NAME", "LAST_NAME", "DOB", "STATUS", "VERSION", "CREATED", "CREATED_BY", "MODIFIED", "MODIFIED_BY")
VALUES ('patient', 'one', '1958-08-29', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('patient', 'two', '1958-08-29', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('patient', 'three', '1958-08-29', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       ('patient', 'four', '1958-08-29', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);


-- PATIENT_HOSPITALS --
INSERT INTO patient_hospitals ("PATIENT_ID", "HOSPITAL_ID", "STATUS", "VERSION", "CREATED", "CREATED_BY", "MODIFIED", "MODIFIED_BY")
VALUES (1, 1, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (2, 1, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (3, 2, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (4, 2, 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);

-- IMAGES --
INSERT INTO IMAGES ("PATIENT_ID", "NAME", "DESCRIPTION", "FILENAME", "RELATIVE_PATH", "THUMBNAIL_RELATIVE_PATH", "STATUS", "VERSION", "CREATED", "CREATED_BY", "MODIFIED", "MODIFIED_BY")
VALUES (1, 'x-ray', 'patient 1 x-ray', '1_1.jpg', 'images/1_1.jpg', 'images/1_1_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 2', '1_2.jpg', 'images/1_2.jpg', 'images/1_2_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 3', '1_3.jpg', 'images/1_3.jpg', 'images/1_3_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 4', '1_4.jpg', 'images/1_4.jpg', 'images/1_4_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 5', '1_5.jpg', 'images/1_5.jpg', 'images/1_5_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 6', '1_6.jpg', 'images/1_6.jpg', 'images/1_6_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 7', '1_7.jpg', 'images/1_7.jpg', 'images/1_7_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 8', '1_8.jpg', 'images/1_8.jpg', 'images/1_8_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 9', '1_9.jpg', 'images/1_9.jpg', 'images/1_9_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (1, 'x-ray', 'patient 1 x-ray 10', '1_10.jpg', 'images/1_10.jpg', 'images/1_10_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (2, 'x-ray', 'patient 2 x-ray', '2_1.jpg', 'images/2_1.jpg', 'images/2_1_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (2, 'x-ray', 'patient 2 x-ray 2', '2_2.jpg', 'images/2_2.jpg', 'images/2_2_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (3, 'x-ray', 'patient 3 x-ray', '3_1.jpg', 'images/3_1.jpg', 'images/3_1_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (3, 'x-ray', 'patient 3 x-ray 2', '3_2.jpg', 'images/3_2.jpg', 'images/3_2_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (4, 'x-ray', 'patient 4 x-ray', '4_1.jpg', 'images/4_1.jpg', 'images/4_1_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (4, 'x-ray', 'patient 4 x-ray 2', '4_2.jpg', 'images/4_2.jpg', 'images/4_2_thmb.jpg', 'ACTIVE', 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);
