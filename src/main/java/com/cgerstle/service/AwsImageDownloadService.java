package com.cgerstle.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class AwsImageDownloadService implements ImageDownloadService
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AmazonS3 s3Client;
    private final String bucket;

    public AwsImageDownloadService(AmazonS3 s3Client, String bucket)
    {
        this.s3Client = s3Client;
        this.bucket = bucket;
    }

    @Override
    public InputStream getImage(String key)
    {
        logger.debug("attempting to download s3://{}/{}", bucket, key);

        GetObjectRequest request = new GetObjectRequest(bucket, key);
        S3Object object = s3Client.getObject(request);

        return object.getObjectContent();
    }
}
