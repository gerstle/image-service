package com.cgerstle.service;

import com.cgerstle.model.request.RequestContext;
import com.cgerstle.model.api.v1.EntityResponse;
import com.cgerstle.model.api.v1.Image;
import com.cgerstle.model.entity.PersistentImage;
import ma.glasnost.orika.MapperFacade;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ImageService
{
    private final static String BASE_QUERY = "SELECT * FROM images i " +
            "LEFT OUTER JOIN patients p ON i.patient_id=p.id " +
            "LEFT OUTER JOIN patient_hospitals ph ON p.id=ph.patient_id ";
    private final static String GET_QUERY = BASE_QUERY + " WHERE i.id=:id";
    private final EntityManager entityManager;
    private final ImageDownloadService imageDownloadService;
    private final MapperFacade mapper;
    private final int defaultLimit;

    public ImageService(EntityManager entityManager, ImageDownloadService imageDownloadService, MapperFacade mapper,
                        int defaultLimit)
    {
        this.entityManager = entityManager;
        this.imageDownloadService = imageDownloadService;
        this.mapper = mapper;
        this.defaultLimit = defaultLimit;
    }

    private PersistentImage fetch(RequestContext requestContext, Long id)
    {
        String queryString = GET_QUERY;
        boolean isAdminAll = requestContext.getContext().getId().equals("all");

        if (!isAdminAll)
            queryString += " AND ph.hospital_id=:hospitalId";

        Query query = entityManager.createNativeQuery(queryString, PersistentImage.class);
        query.setParameter("id", id);

        if (!isAdminAll)
            query.setParameter("hospitalId", requestContext.getContext().getId());

        try
        {
            return (PersistentImage) query.getSingleResult();
        }
        catch (NoResultException ignored)
        {
            return null;
        }
    }

    public Optional<Image> get(RequestContext requestContext, Long id)
    {
        PersistentImage result = fetch(requestContext, id);

        if (result != null)
            return Optional.of(mapper.map(result, Image.class));
        else return Optional.empty();
    }

    public EntityResponse<Image> query(RequestContext requestContext, Optional<Integer> offset,
                                       Optional<Integer> limit, Optional<Long> patientId)
    {
        int queryOffset = offset.isPresent() ? offset.get() : 0;
        int queryLimit = limit.isPresent() ? limit.get() : defaultLimit;

        StringBuilder queryString = new StringBuilder(BASE_QUERY);
        boolean isAdminAll = requestContext.getContext().getId().equals("all");

        if (!isAdminAll)
            queryString.append(" WHERE ph.hospital_id=:hospitalId");

        patientId.ifPresent(p -> {
            if (isAdminAll) queryString.append(" WHERE ");
            else queryString.append(" AND ");

            queryString.append("p.id=:patientId");
        });

        Query query = entityManager
                .createNativeQuery(queryString + " OFFSET :offset LIMIT :limit", PersistentImage.class);
        query.setParameter("offset", queryOffset);
        query.setParameter("limit", queryLimit);
        if (!isAdminAll)
            query.setParameter("hospitalId", requestContext.getContext().getId());
        patientId.ifPresent(p -> query.setParameter("patientId", p));

        List<PersistentImage> results = query.getResultList();
        return new EntityResponse<Image>()
                .setOffset(queryOffset)
                .setResponseCount(results.size())
                .setEntities(results
                        .stream()
                        .map(i -> mapper.map(i, Image.class))
                        .collect(Collectors.toList()));
    }

    public InputStream full(RequestContext requestContext, Long id)
    {
        PersistentImage image = fetch(requestContext, id);
        return imageDownloadService.getImage(image.getRelativePath());
    }

    public InputStream thumbnail(RequestContext requestContext, Long id)
    {
        PersistentImage image = fetch(requestContext, id);
        return imageDownloadService.getImage(image.getThumbnailRelativePath());
    }
}
