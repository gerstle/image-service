package com.cgerstle.service;

import com.cgerstle.model.request.Context;
import com.cgerstle.model.entity.PersistentRole;
import com.cgerstle.model.entity.PersistentUser;
import com.cgerstle.model.exception.AccessDeniedException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class AccessManagementService
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final EntityManager entityManager;
    private final ObjectMapper objectMapper;
    private static final String SELECT_USER_QUERY = "SELECT u FROM PersistentUser u WHERE email=:email";
    private static final String SELECT_ROLES_QUERY = "SELECT * FROM roles r JOIN user_roles ur on r.id=ur.role_id WHERE ur.user_id=:userId";

    public AccessManagementService(EntityManager entityManager, ObjectMapper objectMapper)
    {
        this.entityManager = entityManager;
        this.objectMapper = objectMapper;
    }

    public Optional<PersistentUser> authorize(String email, String password)
    {
        List<PersistentUser> users = entityManager.createQuery(SELECT_USER_QUERY)
                                                  .setParameter("email", email)
                                                  .getResultList();

        if (!CollectionUtils.isEmpty(users))
        {
            PersistentUser user = users.get(0);
            if (checkPassword(password, user.getPassword()))
                return Optional.of(user);
        }

        return Optional.empty();
    }

    private boolean checkPassword(String password_plaintext, String stored_hash)
    {
        boolean password_verified = false;

        if (null == stored_hash || !stored_hash.startsWith("$2a$"))
            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

        password_verified = BCrypt.checkpw(password_plaintext, stored_hash);

        return (password_verified);
    }

    public boolean checkUserContextAccess(Long userId, Context context)
    {
        boolean rv = false;
        List<PersistentRole> roles = entityManager.createNativeQuery(SELECT_ROLES_QUERY, PersistentRole.class)
                                                  .setParameter("userId", userId)
                                                  .getResultList();

        if (CollectionUtils.isEmpty(roles))
            return false;

        // convert current context string into a regex we can match against
        Pattern currentContextPattern = Pattern.compile(String.format("%s/(%s|\\*)", context.getEntity(), context.getId()));

        for (PersistentRole r : roles)
        {
            List<String> allowed = parseContextJsonString(r.getAllowed());
            List<String> denied = parseContextJsonString(r.getDenied());

            // if any denied strings match, return immediately
            for (String c : denied)
            {
                if (currentContextPattern.matcher(c).matches())
                    return false;
            }

            // if any allowed match, just set return value to true as we might hit a denied on a separate role
            for (String c : allowed)
            {
                if (currentContextPattern.matcher(c).matches())
                    rv = true;
            }
        }

        return rv;
    }

    private List<String> parseContextJsonString(String contextJsonString)
    {
        if (!StringUtils.isEmpty(contextJsonString))
        {
            try
            {
                return objectMapper.readValue(contextJsonString, List.class);
            }
            catch (IOException e)
            {
                logger.warn("failed to parse context string: {}", contextJsonString);
                throw new AccessDeniedException();
            }
        }


        return Collections.EMPTY_LIST;
    }
}
