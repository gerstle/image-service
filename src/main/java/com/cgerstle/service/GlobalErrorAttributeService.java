package com.cgerstle.service;

import com.cgerstle.model.exception.ResponseException;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;

public class GlobalErrorAttributeService<T extends Throwable> extends DefaultErrorAttributes
{
    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace)
    {
        Map<String, Object> map = super.getErrorAttributes(request, includeStackTrace);

        Throwable t = getError(request);

        if (t instanceof ResponseException)
        {
            ResponseException re = (ResponseException) t;
            map.put("status", re.getStatusCode());
            map.put("error", re.getReason());
        }
        return map;
    }

}
