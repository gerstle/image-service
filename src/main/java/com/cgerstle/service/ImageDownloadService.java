package com.cgerstle.service;

import java.io.InputStream;

public interface ImageDownloadService
{
    InputStream getImage(String relativePath);
}
