package com.cgerstle.model.exception;

import org.springframework.http.HttpStatus;

public class NoContentException extends RuntimeException implements ResponseException
{
    @Override
    public int getStatusCode()
    {
        return HttpStatus.NO_CONTENT.value();
    }

    @Override
    public String getReason()
    {
        return "No Content";
    }
}
