package com.cgerstle.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class AuthorizationMissingException extends RuntimeException implements ResponseException
{
    @Override
    public int getStatusCode()
    {
        return HttpStatus.BAD_REQUEST.value();
    }

    @Override
    public String getReason()
    {
        return "Authorization missing";
    }
}
