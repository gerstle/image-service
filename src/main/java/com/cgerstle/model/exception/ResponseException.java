package com.cgerstle.model.exception;

public interface ResponseException
{
    int getStatusCode();
    String getReason();
}
