package com.cgerstle.model.exception;

import org.springframework.http.HttpStatus;

public class AccessDeniedException extends RuntimeException implements ResponseException
{
    @Override
    public int getStatusCode()
    {
        return HttpStatus.UNAUTHORIZED.value();
    }

    @Override
    public String getReason()
    {
        return "Access Denied";
    }
}
