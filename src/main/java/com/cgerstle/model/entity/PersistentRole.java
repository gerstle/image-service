package com.cgerstle.model.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
@DynamicUpdate
public class PersistentRole extends BasePersistentEntity
{
    private String name;
    private String description;
    private String allowed;
    private String denied;

    public String getName()
    {
        return name;
    }

    public PersistentRole setName(String name)
    {
        this.name = name;
        return this;
    }

    public String getDescription()
    {
        return description;
    }

    public PersistentRole setDescription(String description)
    {
        this.description = description;
        return this;
    }

    public String getAllowed()
    {
        return allowed;
    }

    public PersistentRole setAllowed(String allowed)
    {
        this.allowed = allowed;
        return this;
    }

    public String getDenied()
    {
        return denied;
    }

    public PersistentRole setDenied(String denied)
    {
        this.denied = denied;
        return this;
    }
}
