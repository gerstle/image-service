package com.cgerstle.model.entity;

import org.hibernate.annotations.OptimisticLock;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class BasePersistentEntity
{
    @Id
    private Long id;
    @OptimisticLock(excluded = true)
    private String status;
    @Version
    private Integer version;
    @Temporal(TemporalType.TIMESTAMP)
    @OptimisticLock(excluded = true)
    private Date created;
    private Long created_by;
    @Temporal(TemporalType.TIMESTAMP)
    @OptimisticLock(excluded = true)
    private Date modified;
    private Long modified_by;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public Date getCreated()
    {
        return created;
    }

    public void setCreated(Date created)
    {
        this.created = created;
    }

    public Long getCreated_by()
    {
        return created_by;
    }

    public void setCreated_by(Long created_by)
    {
        this.created_by = created_by;
    }

    public Date getModified()
    {
        return modified;
    }

    public void setModified(Date modified)
    {
        this.modified = modified;
    }

    public Long getModified_by()
    {
        return modified_by;
    }

    public void setModified_by(Long modified_by)
    {
        this.modified_by = modified_by;
    }
}
