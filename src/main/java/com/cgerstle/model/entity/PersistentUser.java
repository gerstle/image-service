package com.cgerstle.model.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@DynamicUpdate
public class PersistentUser extends BasePersistentEntity
{
    private String email;
    private String password;

    public String getEmail()
    {
        return email;
    }

    public PersistentUser setEmail(String email)
    {
        this.email = email;
        return this;
    }

    public String getPassword()
    {
        return password;
    }

    public PersistentUser setPassword(String password)
    {
        this.password = password;
        return this;
    }
}
