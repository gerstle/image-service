package com.cgerstle.model.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "images")
@DynamicUpdate
public class PersistentImage extends BasePersistentEntity
{
    private Long patientId;
    private String name;
    private String description;
    private String filename;
    private String relativePath;
    private String thumbnailRelativePath;

    public Long getPatientId()
    {
        return patientId;
    }

    public PersistentImage setPatientId(Long patientId)
    {
        this.patientId = patientId;
        return this;
    }

    public String getName()
    {
        return name;
    }

    public PersistentImage setName(String name)
    {
        this.name = name;
        return this;
    }

    public String getDescription()
    {
        return description;
    }

    public PersistentImage setDescription(String description)
    {
        this.description = description;
        return this;
    }

    public String getFilename()
    {
        return filename;
    }

    public PersistentImage setFilename(String filename)
    {
        this.filename = filename;
        return this;
    }

    public String getRelativePath()
    {
        return relativePath;
    }

    public PersistentImage setRelativePath(String relativePath)
    {
        this.relativePath = relativePath;
        return this;
    }

    public String getThumbnailRelativePath()
    {
        return thumbnailRelativePath;
    }

    public PersistentImage setThumbnailRelativePath(String thumbnailRelativePath)
    {
        this.thumbnailRelativePath = thumbnailRelativePath;
        return this;
    }
}
