package com.cgerstle.model.request;

import com.cgerstle.model.entity.PersistentUser;
import org.springframework.http.server.reactive.ServerHttpRequest;

public class RequestContext
{
    private ServerHttpRequest request;
    private PersistentUser user;
    private Context context;

    public ServerHttpRequest getRequest()
    {
        return request;
    }

    public RequestContext setRequest(ServerHttpRequest request)
    {
        this.request = request;
        return this;
    }

    public PersistentUser getUser()
    {
        return user;
    }

    public RequestContext setUser(PersistentUser user)
    {
        this.user = user;
        return this;
    }

    public Context getContext()
    {
        return context;
    }

    public RequestContext setContext(Context context)
    {
        this.context = context;
        return this;
    }
}
