package com.cgerstle.model.request;

public class Context
{
    private final String entity;
    private final String id;

    public Context(String entity, String id)
    {
        this.entity = entity;
        this.id = id;
    }

    public String getEntity()
    {
        return entity;
    }

    public String getId()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return "Context{" +
                "entity='" + entity + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
