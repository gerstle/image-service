package com.cgerstle.model.api;

public class BaseApiEntity<ID>
{
    private ID id;

    public ID getId()
    {
        return id;
    }

    public BaseApiEntity<ID> setId(ID id)
    {
        this.id = id;
        return this;
    }
}
