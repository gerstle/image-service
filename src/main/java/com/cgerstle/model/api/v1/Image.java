package com.cgerstle.model.api.v1;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"id", "patientId", "name", "description", "filename", "status", "version", "created", "createdBy", "modified", "modifiedBy"})
public class Image extends BaseV1ApiEntity
{
    private Long patientId;
    private String name;
    private String description;
    private String filename;

    public Long getPatientId()
    {
        return patientId;
    }

    public Image setPatientId(Long patientId)
    {
        this.patientId = patientId;
        return this;
    }

    public String getName()
    {
        return name;
    }

    public Image setName(String name)
    {
        this.name = name;
        return this;
    }

    public String getDescription()
    {
        return description;
    }

    public Image setDescription(String description)
    {
        this.description = description;
        return this;
    }

    public String getFilename()
    {
        return filename;
    }

    public Image setFilename(String filename)
    {
        this.filename = filename;
        return this;
    }
}
