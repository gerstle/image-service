package com.cgerstle.model.api.v1;

import java.util.List;

public class EntityResponse<C>
{
    private Integer offset;
    private Integer responseCount;
    private List<C> entities;

    public Integer getOffset()
    {
        return offset;
    }

    public EntityResponse<C> setOffset(Integer offset)
    {
        this.offset = offset;
        return this;
    }

    public Integer getResponseCount()
    {
        return responseCount;
    }

    public EntityResponse<C> setResponseCount(Integer responseCount)
    {
        this.responseCount = responseCount;
        return this;
    }

    public List<C> getEntities()
    {
        return entities;
    }

    public EntityResponse<C> setEntities(List<C> entities)
    {
        this.entities = entities;
        return this;
    }
}
