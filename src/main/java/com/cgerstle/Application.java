package com.cgerstle;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableConfigurationProperties
@EnableAutoConfiguration
public class Application
{
    public static void main(String... args)
    {
        // depending on your operational model, this might should be wrapped in a try/catch
        // so spring configuration errors encountered on startup aren't thrown to stdout
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Application.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }
}
