package com.cgerstle.controller;

import com.cgerstle.model.request.Context;
import com.cgerstle.model.request.RequestContext;
import com.cgerstle.model.entity.PersistentUser;
import com.cgerstle.model.exception.AccessDeniedException;
import com.cgerstle.model.exception.AuthorizationMissingException;
import com.cgerstle.service.AccessManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import java.util.Base64;
import java.util.Optional;

public class BaseAuthorizingController
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AccessManagementService acm;

    public BaseAuthorizingController(AccessManagementService acm)
    {
        this.acm = acm;
    }

    RequestContext authorize(ServerWebExchange exchange)
    {
        // first validate auth header and fetch user
        String authorization = exchange.getRequest().getHeaders().getFirst("authorization");
        if (StringUtils.isEmpty(authorization))
            throw new AuthorizationMissingException();

        Optional<PersistentUser> maybeUser;
        try
        {
            String b64Auth = authorization.replace("Basic ", "");
            String decoded = new String(Base64.getDecoder().decode(b64Auth));
            String[] tokens = StringUtils.tokenizeToStringArray(decoded, ":");

            maybeUser = acm.authorize(tokens[0], tokens[1]);
        }
        catch (Exception ignored)
        {
            throw new AccessDeniedException();
        }

        maybeUser.orElseThrow(AccessDeniedException::new);

        // now let's validate that this user has access to the provided context
        Context context = getContext(exchange.getRequest().getQueryParams().getFirst("context"));
        logger.debug("user: {}, context provided: {}", maybeUser.get().getEmail(), context);
        if (!acm.checkUserContextAccess(maybeUser.get().getId(), context))
            throw new AccessDeniedException();

        return new RequestContext()
                .setRequest(exchange.getRequest())
                .setUser(maybeUser.get())
                .setContext(context);
    }

    private Context getContext(String contextString)
    {
        try
        {
            String[] contextTokens = StringUtils.tokenizeToStringArray(contextString, "/");
            return new Context(contextTokens[0], contextTokens[1]);
        }
        catch (Exception e)
        {
            logger.info("exception occurred while parsing context", e);
            throw new AccessDeniedException();
        }
    }
}
