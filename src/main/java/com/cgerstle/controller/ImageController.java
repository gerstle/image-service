package com.cgerstle.controller;

import com.amazonaws.util.IOUtils;
import com.cgerstle.model.request.RequestContext;
import com.cgerstle.model.api.v1.EntityResponse;
import com.cgerstle.model.api.v1.Image;
import com.cgerstle.model.exception.NoContentException;
import com.cgerstle.service.AccessManagementService;
import com.cgerstle.service.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.io.InputStream;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/images", produces = MediaType.APPLICATION_JSON_VALUE)
public class ImageController extends BaseAuthorizingController
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Scheduler scheduler;
    private final ImageService imageService;

    public ImageController(AccessManagementService acm, Scheduler scheduler, ImageService imageService)
    {
        super(acm);
        this.scheduler = scheduler;
        this.imageService = imageService;
    }

    @GetMapping(path = "{id}")
    public Mono<Image> getImage(@PathVariable(name = "id") Long id,
                                ServerWebExchange exchange)
    {
        return Mono.defer(() -> {
            RequestContext requestContext = authorize(exchange);
            Optional<Image> result = imageService.get(requestContext, id);

            if (result.isPresent())
                return Mono.just(result.get());
            else
                return Mono.empty();
        }).subscribeOn(scheduler);
    }

    @GetMapping
    public Mono<EntityResponse<Image>> queryImages(@RequestParam(name = "offset") Optional<Integer> offset,
                                                   @RequestParam(name = "limit") Optional<Integer> limit,
                                                   @RequestParam(name = "patientId") Optional<Long> patientId,
                                                   ServerWebExchange exchange)
    {
        return Mono.defer(() -> {
            RequestContext requestContext = authorize(exchange);
            return Mono.just(imageService.query(requestContext, offset, limit, patientId));
        }).subscribeOn(scheduler);
    }

    @GetMapping(path = "{id}/full", produces = MediaType.IMAGE_JPEG_VALUE)
    public Mono<ResponseEntity<byte[]>> downloadImage(@PathVariable(name = "id") Long id, ServerWebExchange exchange)
    {
        return Mono.defer(() -> {
            RequestContext requestContext = authorize(exchange);

            InputStream in = imageService.full(requestContext, id);
            byte[] media;
            try
            {
                media = IOUtils.toByteArray(in);
            }
            catch (Exception e)
            {
                logger.error("exception while converting image to bytes", e);
                throw new NoContentException();
            }

            return Mono.just(new ResponseEntity<>(media, HttpStatus.OK));
        }).subscribeOn(scheduler);
    }

    @GetMapping(path = "{id}/thumbnail", produces = MediaType.IMAGE_JPEG_VALUE)
    public Mono<ResponseEntity<byte[]>> downloadImageThumbnail(@PathVariable(name = "id") Long id, ServerWebExchange exchange)
    {
        return Mono.defer(() -> {
            RequestContext requestContext = authorize(exchange);

            InputStream in = imageService.thumbnail(requestContext, id);
            byte[] media;
            try
            {
                media = IOUtils.toByteArray(in);
            }
            catch (Exception e)
            {
                logger.error("exception while converting image to bytes", e);
                throw new NoContentException();
            }

            return Mono.just(new ResponseEntity<>(media, HttpStatus.OK));
        }).subscribeOn(scheduler);
    }
}
