package com.cgerstle.config;

import com.cgerstle.model.api.v1.Image;
import com.cgerstle.model.entity.PersistentImage;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TranslationConfigBeans
{
    @Bean
    MapperFactory mapperFactory()
    {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

        mapperFactory.classMap(PersistentImage.class, Image.class)
                     .byDefault()
                     .register();

        return mapperFactory;
    }
}
