package com.cgerstle.config;

import com.cgerstle.service.AccessManagementService;
import com.cgerstle.service.GlobalErrorAttributeService;
import com.cgerstle.service.ImageDownloadService;
import com.cgerstle.service.ImageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class ServiceConfigBeans
{
    @Bean
    GlobalErrorAttributeService globalErrorAttributeService()
    {
        return new GlobalErrorAttributeService();
    }

    @Bean
    AccessManagementService accessManagementService(EntityManager entityManager, ObjectMapper objectMapper)
    {
        return new AccessManagementService(entityManager, objectMapper);
    }

    @Bean
    ImageService imageService(EntityManager entityManager, ImageDownloadService imageDownloadService,
                              MapperFactory mapperFactory,
                              @Value("${service.defaultLimit}") int defaultLimit)
    {
        return new ImageService(entityManager, imageDownloadService, mapperFactory.getMapperFacade(), defaultLimit);
    }
}
