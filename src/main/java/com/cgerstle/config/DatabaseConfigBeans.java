package com.cgerstle.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfigBeans
{
    @Bean
    @ConfigurationProperties(prefix = "database")
    public DataSource h2DataSource() {
        return new DriverManagerDataSource();
    }
}
