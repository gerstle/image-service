package com.cgerstle.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.Executor;

@Configuration
public class SchedulerConfigBeans
{
    @Bean
    Executor imageExecutor(@Value("${service.image.maxThreads}") int threads,
                           @Value("${service.image.maxQueueSize}") int maxQueueSize)
    {
        return buildExecutor(threads, maxQueueSize, "image-");
    }

    @Bean
    Scheduler imageScheduler(Executor registrationExecutor)
    {
        return Schedulers.fromExecutor(registrationExecutor);
    }

    private Executor buildExecutor(int threads, int maxQueueSize, String prefix)
    {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(threads);
        taskExecutor.setMaxPoolSize(threads);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setQueueCapacity(maxQueueSize);
        taskExecutor.setThreadFactory(new CustomizableThreadFactory(prefix));
        return taskExecutor;
    }
}
