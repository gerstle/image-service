package com.cgerstle.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.cgerstle.service.AwsImageDownloadService;
import com.cgerstle.service.ImageDownloadService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IntegrationConfigBeans
{
    @Bean
    AmazonS3 amazonS3(@Value("${aws.region}") String region,
                      @Value("${aws.access-key-id}") String accessKey,
                      @Value("${aws.secret-access-key}") String secretKey)
    {
        return AmazonS3ClientBuilder.standard()
                                    .withRegion(region)
                                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                                    .build();
    }

    // TODO - Could dynamically create AWS / Google service here dependent upon configs, etc
    @Bean
    ImageDownloadService awsImageDownloadService(AmazonS3 amazonS3,
                                                 @Value("${aws.s3.bucket}") String bucket)
    {
        return new AwsImageDownloadService(amazonS3, bucket);
    }
}
