package com.cgerstle;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class Blah
{
    public static void main(String... args)
    {

        System.out.println(BCrypt.hashpw("boatdrinks", BCrypt.gensalt(12)));
    }
}
